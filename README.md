# Dynamic Library Loader
  * Uses C++ classes to simplify Library dynamic loader loading.
  * Cross platform (windows and linux).
  * Uniform interface to load shared library and call functions on the fly.

## How it works
  * The library has two similar version, a thread safe one and non-thread safe one.
  * The class `DynLibLoader` is non-thread safe part implemented as a singleton pattern.
  * The class `ThreadSafeLoader` is a thread safe wrapper to `DynLibLoader`.
  * `DynLibLoader` implements the singleton design pattern by exposing a static function called
  `instance` to create references to the single statically created version.
  * When load method is called it accepts, a string with the name of the library to load.
  for linux we have to provide the full name of the library including version like (libm.so.6) 
  but on windows we can exclude the trailing .dll extension and windows will be fine. it
  returns a Function resolver class. This class implements a resolve method with singnature like
  `FuncResolver<LibHandleType>::resolver<ReturnType>(FuncName)` where LibHandleType is a type template 
  argument that is the type of a lib handle. For linux its `void*` for windows its `HANDLE`. `ReturnType`
  is type template paramter that specifies the return type of the function to call. FuncName is a string
  specifiying the function to resolve in the library. The resolve method returns a Callable object that
  overloads the braces operator with signature 
  `Callable<ReturnType, FuncHandle>::operator()<Args...>(Args&&...)` where `ReturnType` is same as before
  `FuncHandle`  is the function handle type (`void*` on linux `FARPROC` on windows). It accepts an 
  arbitrary number of arguments whose template arguments are auto deduced due to C++17's function template
  deduction feature. So to call a function like `cos` with signature `double cos(double)` we just pass
  the argument and it will auto deduce the template.


## Using dynamic library loader, Non thread Safe version
```cpp
    // must return a reference since constructer is inaccessible. Cannot move
    // or Copy either since all move and copy constructors are deleted.
    auto& loader = DynLibLoader::instance();
    auto fresolver = loader.load("libm.so.6");
    auto Cos = fresolver.resolve<double>("cos");
    auto Pow = fresolver.resolve<double>("pow");
    std::cout << Pow(100.0,0.5) << '\n';
    std::cout << Cos(10.0) << '\n';
```

## Using dynamic library loader, thread safe version
```cpp
    // Thread safe version can be constucted like normal objects, because the underlying
    // non thread safe version is a singleton so all instances of the copy of the
    // thread safe version will use the same non-thread version.
    ThreadSafeLoader loader{};
    auto fresolver = loader.load("libm.so.6");
    auto Cos = fresolver.resolve<double>("cos");
    auto Pow = fresolver.resolve<double>("pow");
    std::cout << Pow(100.0,0.5) << '\n';
    std::cout << Cos(10.0) << '\n';
```

## Building on linux
```bash
    g++ -o {OUTFILE} src/main.cpp -ldl
    # -ldl is required to link dlfcn.h's shared object into executable.
```

## Using native api (Linux) to load library
```cpp
    double (*cos)(double);
    double (*pow)(double, double);
    auto handle = dlopen("libm.so.6", RTLD_LAZY);
    if (handle == nullptr) {
        throw std::invalid_argument{"Cannot find library"};
    }
    cos = (double (*)(double))dlsym(handle, "cos");
    if (cos == nullptr) {
        dlclose(handle);
        throw std::invalid_argument{"Function cos does not exist"};
    }
    pow = (double (*)(double, double))dlsym(handle, "pow");
    if (pow == nullptr) {
        dlclose(handle);
        throw std::invalid_argument{"Function pow does not exist"};
    }
    std::cout << cos(10.0) << '\n';
    std::cout << pow(100.0, 0.5) << '\n';
    dlclose(handle);
```

## Using native api (Windows)
```cpp
    using DWORD = unsigned long;
    DWORD(*_GetTickCount64)();
    HANDLE handle = LoadLibraryA("kernel32.dll");
    if (handle == nullptr) {
        throw std::invalid_argument{"kernel32.dll cannot be found"};
    }
    FARPROC func = GetProcAddress(handle,"GetTickCount64");
    if (func == nullptr) {
        throw std::invalid_argument{"GetTickCount64 cannot be found in kernel32.dll"};
    }
    _GetTickCount64 = (double(*)())func;
    std::cout << "Tick Count: " << _GetTickCount64() << std::endl;
```
