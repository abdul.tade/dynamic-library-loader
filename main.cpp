# if defined(_WIN32)
# include <Windows.h>
# endif

# if defined(__linux__)
# include <unistd.h>
# include <dlfcn.h>
# endif

# ifndef __cplusplus
# error Source must be compiled with C++
# endif

# include <iostream>
# include <vector>
# include <map>
# include <memory>
# include <mutex>
# include <chrono>

/* A callable that allows the function handle passed to be
invoked */
template <typename ReturnType, typename FuncHandle>
struct Callable {

    Callable(FuncHandle handle)
        : _handle(handle)
    {
        if (_handle == nullptr) {
            throw std::invalid_argument{ "Function handle cannot be null" };
        }
    }

    template <typename... Args>
    auto operator()(Args&&... args) {
        using FuncType = ReturnType(*)(Args...);
        FuncType func = reinterpret_cast<FuncType>(_handle);
        return func(args...);
    }

private:
    FuncHandle _handle{};
};

/*
    Resolves function names to function handles and returns a callable that
    can be invoked to execute the given function from the handle
*/
template <typename LibHandleType>
class FuncResolver {

public:
    FuncResolver(LibHandleType handle)
        : _libHandle(handle)
    {
        if (_libHandle == nullptr) {
            throw std::invalid_argument{ "library handle must not be a null" };
        }
    }

    ~FuncResolver() noexcept = default;

    template <typename ReturnType>
    auto resolve(const std::string& funcName)
    {
        auto funcHandle = getFuncHandle(funcName);
        Callable<ReturnType, decltype(funcHandle)> callable{ funcHandle };
        return callable;
    }

private:

    LibHandleType _libHandle;

    auto getFuncHandle(const std::string& funcName) {
# if defined(_WIN32)
        auto fhandle = GetProcAddress(_libHandle, funcName.c_str());
# endif
# if defined(__linux__)
        auto fhandle = dlsym(_libHandle, funcName.c_str());
# endif
        if (fhandle == nullptr) {
            throw std::invalid_argument{ "cannot find function in library" };
        }
        return fhandle;
    }
};

/* Contains and tracks all opened library handles
In the destructor all dll handles are closed*/
class DynLibLoader {

private:
# if defined(_WIN32)
    std::map<std::string, HMODULE> FuncMap;
    using HandleType = HMODULE;
# endif

# if defined(__linux__)
    std::map<std::string, void*> FuncMap;
    using HandleType = void*;
# endif

    void freeLib(HandleType handle) {
# if defined(_WIN32)
        FreeLibrary(handle);
# endif

# if defined(__linux__)
        dlclose(handle);
# endif
    }

    DynLibLoader() = default;

public:

    /*To make sure DynLibLoader class cannot be copied*/
    DynLibLoader(const DynLibLoader&) = delete;
    DynLibLoader& operator=(const DynLibLoader&) = delete;

    /*Instantiate singleton class */
    static DynLibLoader& instance() {
        static auto ld = std::shared_ptr<DynLibLoader>(new DynLibLoader);
        return *ld;
    }

    ~DynLibLoader() {
        for (auto [name, handle] : FuncMap)
        {
            if (handle != nullptr) {
                freeLib(handle);
            }
        }
    }

    auto load(const std::string& libName)
    {
        if (!FuncMap.count(libName)) {

# if defined(_WIN32)
            auto h = LoadLibraryA(libName.c_str());
# endif

# if defined(__linux__)
            auto h = dlopen(libName.c_str(), RTLD_LAZY);
# endif

            if (h == nullptr) {
                throw std::invalid_argument{ "library does not exist" };
            }

            FuncMap.insert(std::pair<std::string, HandleType>{libName, h});
            return FuncResolver{ h };
        }

        return FuncResolver{ FuncMap.at(libName) };
    }
};

 class ThreadSafeLoader 
 {
     private:

         std::mutex _mtx;
         DynLibLoader &_loader;

     public:

         ThreadSafeLoader()
             : _loader(DynLibLoader::instance())
         {}

         ~ThreadSafeLoader() =  default;

         auto load(const std::string &libName)
         {
             std::lock_guard<std::mutex> lck{_mtx};
             return _loader.load(libName);
         }
 };

int main(int argc, char* argv[])
{
# if defined(_WIN32)
    auto& loader = DynLibLoader::instance();
    std::string libName{ "kernel32" };
    auto fresolver = loader.load(libName);

    auto TickCount = fresolver.resolve<ULONGLONG>("GetTickCount64");
    auto ThreadId = fresolver.resolve<DWORD>("GetCurrentThreadId");
    auto openThread = fresolver.resolve<HANDLE>("OpenThread");
    auto ThreadCtx = fresolver.resolve<BOOL>("GetThreadContext");
    auto suspendThread = fresolver.resolve<DWORD>("SuspendThread");
    auto closeHandle = fresolver.resolve<void>("CloseHandle");

    auto handle = openThread(THREAD_ALL_ACCESS, FALSE, ThreadId());

    CONTEXT ctx;
    auto result = ThreadCtx(handle, &ctx);
    std::cout << "RAX: " << std::hex << ctx.Rax << "\n";
    std::cout << "HANDLE: " << handle << "\n";
    closeHandle(handle);
# endif

# if defined(__linux__)
    auto& loader = DynLibLoader::instance();
    std::string libName{ "libc.so.6" };
    auto fresolver = loader.load(libName);
    auto Printf = fresolver.resolve<int>("printf");

    int age = 100;
    //passing variable to callable does not work unless perfect forwarding is applied
    Printf("\nI am Abdul Hameed. I am %d years old\n", std::forward<int>(age));
    std::cout << age;
# endif
}